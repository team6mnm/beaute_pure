<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
    <!-- SEO Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="distribution" content="global">
    <meta name="revisit-after" content="2 Days">
    <meta name="robots" content="ALL">
    <meta name="rating" content="8 YEARS">
    <meta name="Language" content="en-us">
    <meta name="GOOGLEBOT" content="NOARCHIVE">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css"
          integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/fotorama.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/responsive.css">
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory') ?>/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory') ?>/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php bloginfo('stylesheet_directory') ?>/images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php bloginfo('stylesheet_directory') ?>/images/apple-touch-icon-114x114.html">
    <!-- JS FILES STARTS -->
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/jquery-1.12.3.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/jquery-ui.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/fotorama.js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.magnific-popup.js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/js/custom.js"></script>
</head>
<body class="homepage">
<div class="se-pre-con"></div>
<!-- MAIN START -->
<div class="main">
    <!-- HEADER START -->
    <header class="navbar navbar-custom " id="header">
        <!-- CONTAINER START -->
        <div class="container">
            <!-- ROW START -->
            <div class="row">
                <!-- HEADER LOGO START-->
                <div class="col-md-3 col-xs-6">
                    <div class="navbar-header">
                        <a class="logo" href="index.html">
                            <img alt="logo" src="<?php bloginfo( 'stylesheet_directory' ) ?>/images/logobq.png">
                        </a>
                    </div>
                </div>
                <!-- HEADER LOGO END-->
                <!-- HEADER ICONS START-->
                <div class="col-md-3 col-xs-6 right-side">
                    <div class="header-right-link right-side float-none-xs">
                        <ul>
                            <li class="search-box">
                                <a href="#">
                                    <span></span>
                                </a>
                            </li>
                            <li class="account-icon">
                                <a href="#">
                                    <span></span>
                                </a>
                                <div class="header-link-dropdown account-link-dropdown">
                                    <ul class="link-dropdown-list">
                                        <li>
                          <span class="dropdown-title">
                            Accounts menu
                          </span>
                                            <ul>
                                                <li>
                                                    <a href="<?php bloginfo( 'url' ) ?>/wp-admin">Dashboard</a>
                                                </li>
                                                <li>
                                                    <a href="login.html">Sign In</a>
                                                </li>
                                                <li>
                                                    <a href="register.html">Create an Account</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="cart-icon">
                                <a href="<?php bloginfo( 'url' ); ?>/gio-hang">
                      <span>
                        <small class="cart-notification">
	                        <?php if ( isset( $_SESSION['cart'] ) && count( $_SESSION['cart'] ) > 0 ) {
		                        echo count( $_SESSION['cart'] );
	                        } ?>
                        </small>
                      </span>
                                </a>
                                <div class="header-link-dropdown cart-dropdown">
                                    <ul class="cart-list link-dropdown-list">
	                                    <?php
	                                    if ( ! isset( $_SESSION['cart'] ) || ( isset( $_SESSION['cart'] ) && count( $_SESSION['cart'] ) == 0 ) ) { ?>
                                            <p>There are no products in your shopping cart!</p>
	                                    <?php } else {
	                                    global $product_prefix;
	                                    $price_total = 0;
	                                    foreach ( $_SESSION['cart'] as $pro_id => $quantity ) { //lặp và hiển thị thông tin sản phẩm trong giỏ
	                                    $product = get_post( $pro_id );
	                                    $sanpham = new WP_Query( array( 'p' => $pro_id ) );
	                                    while ( $sanpham->have_posts() ) {
	                                    $sanpham->the_post();
	                                    $hinhanh = get_field( 'hinhanh_sp' );
	                                    $ha      = $hinhanh['hinh_chinh']['url'];
	                                    $price   = get_field( 'gia_ban' );

	                                    if ( $price && $price != "" ) {
		                                    $price_total += ( (float) $price * $quantity );
	                                    } else {
		                                    echo " $0";
	                                    }
	                                    ?>
                                        <li>
                                            <div class="media">
                                                <a class="pull-left" href="<?php the_permalink() ?>">
                                                    <img alt="anhsp"
                                                         src="<?= $ha ?>">
                                                </a>
                                                <div class="media-body">
                                              <span>
                                                <a href="<?php the_permalink() ?>"><?php echo $product->post_title; ?></a>
                                              </span>
                                                    <p class="cart-price">$<?= $price ?></p>
                                                    <div class="product-qty">
                                                        <label>Qty:</label>
                                                        <div class="custom-qty">
                                                            <p><?= $quantity ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <p class="cart-sub-totle">
                                        <span class="pull-left">Cart Subtotal</span>
                                        <span class="pull-right">
                          <strong class="price-box">$<?= $price_total ?></strong>
	                                        <?php }
	                                        wp_reset_query();
	                                        }
	                                        } ?>
                        </span>
                                    </p>
                                    <div class="clearfix"></div>
                                    <div class="mt-20">
                                        <a href="<?php bloginfo( 'url' ) ?>/gio-hang" class="btn-color btn">Cart</a>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <!-- HEADER TOGGLE BUTTON START-->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- HEADER TOGGLE BUTTON END-->
                </div>
                <!-- HEADER ICONS END-->
                <!-- HEADER MENU START-->
                <div class="col-md-6 col-sm-12 position-s left-side float-none-xs">
                    <div class="align-center">
	                    <?= wp_nav_menu( array(
		                    'container'       => 'div',
		                    'container_class' => 'navbar-collapse collapse',
		                    'menu_class'      => 'nav navbar-nav',
	                    ) ) ?>
                    </div>
                </div>
                <!-- HEADER MENU END-->
            </div>
            <!-- ROW END -->
        </div>
        <!-- CONTAINER END -->
    </header>
    <!-- HEADER END -->

    <!-- SEARCH POPUP START -->
    <div class="sidebar-search-wrap">
        <div class="sidebar-table-container">
            <div class="sidebar-align-container">
                <div class="search-closer right-side"></div>
                <div class="search-container">
                    <form method="get" id="search-form">
                        <input type="text" id="s" class="search-input" name="s" placeholder="Start Searching">
                    </form>
                    <span>Search and Press Enter</span>
                </div>
            </div>
        </div>
    </div>
    <!-- SEARCH POPUP ENDS -->

