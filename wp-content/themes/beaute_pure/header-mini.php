<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- SEO Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="distribution" content="global">
    <meta name="revisit-after" content="2 Days">
    <meta name="robots" content="ALL">
    <meta name="rating" content="8 YEARS">
    <meta name="Language" content="en-us">
    <meta name="GOOGLEBOT" content="NOARCHIVE">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css"
          integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/fotorama.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_directory' ) ?>/css/responsive.css">
    <link rel="shortcut icon" href="<?php bloginfo( 'stylesheet_directory' ) ?>/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php bloginfo( 'stylesheet_directory' ) ?>/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php bloginfo( 'stylesheet_directory' ) ?>/images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php bloginfo( 'stylesheet_directory' ) ?>/images/apple-touch-icon-114x114.html">
    <!-- JS FILES STARTS -->
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/jquery-1.12.3.min.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/jquery-ui.min.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/fotorama.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/jquery.magnific-popup.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ) ?>/js/custom.js"></script>
</head>
<body class="homepage">
<div class="se-pre-con"></div>
<!-- MAIN START -->
<div class="main">