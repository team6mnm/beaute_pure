<?php
$slide1 = get_field( 'slide_1' );
$slide2 = get_field( 'slide_2' );
$slide3 = get_field( 'slide_3' );
?>
<div class="banner">
    <div class="main-banner">
        <div class="banner-1"><img src="<?= $slide1['anh_sl']['url'] ?>" alt="<?= $slide1['anh_sl']['alt'] ?>">
            <div class="banner-detail">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-detail-inner">
                                <span class="slogan"><?= $slide1['span_sl'] ?></span>
                                <h1 class="banner-title"><?= $slide1['h1_sl'] ?></h1>
                            </div>
                            <a class="btn btn-color big"
                               href="<?= $slide1['btn-link_sl'] ?>"><?= $slide1['btn-text_sl'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-2"><img src="<?= $slide2['anh_sl']['url'] ?>" alt="<?= $slide2['anh_sl']['alt'] ?>">
            <div class="banner-detail">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-detail-inner">
                                <span class="slogan"><?= $slide2['span_sl'] ?></span>
                                <h1 class="banner-title"><?= $slide2['h1_sl'] ?></h1>
                            </div>
                            <a class="btn btn-color big"
                               href="<?= $slide2['btn-link_sl'] ?>"><?= $slide2['btn-text_sl'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-3"><img src="<?= $slide3['anh_sl']['url'] ?>" alt="<?= $slide3['anh_sl']['alt'] ?>">
            <div class="banner-detail">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-detail-inner">
                                <span class="slogan"><?= $slide3['span_sl'] ?></span>
                                <h1 class="banner-title"><?= $slide3['h1_sl'] ?></h1>
                            </div>
                            <a class="btn btn-color big"
                               href="<?= $slide3['btn-link_sl'] ?>"><?= $slide3['btn-text_sl'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>