<?php
get_header();
?>

<!-- BANNER STRAT -->
<div class="banner inner-banner1">
    <div class="container">
        <section class="banner-detail ptb-95">
            <h1 class="banner-title">Product</h1>
            <div class="bread-crumb">
                <ul>
                    <li><a href="<?= bloginfo( 'home' ) ?>">Home</a>/</li>
                    <li><span>Product</span></li>
                </ul>
            </div>
        </section>
    </div>
</div>
<!-- BANNER END -->

<!-- FEATURED PRODUCT AREA START -->
<section class="ptb-95">
    <div class="container">
        <!-- PRODUCT-LISTING MAIN CLASS START -->
        <div class="product-listing">
            <!-- TAB SECTION CONTENT START -->
            <div class="row mlr_-20">
                <div id="items">
                    <div class="tab_content pro_cat">
                        <div class="items-step1 product-slider-main position-r selected" data-temp="tabdata">
							<?php
							while ( have_posts() ) {
								the_post();
								$tinh_trang = get_field_object( 'tinh_trang' );
								$khuyenmai  = get_field( 'km' );
								$giaban     = get_field( 'gia_ban' );
								$giagoc     = get_field( 'gia_goc' );
								$hinhanh    = get_field( 'hinhanh_sp' );
								?>
                                <div class="col-lg-3 col-sm-4 col-xs-6 plr-20 mb-30">
                                    <div class="product-item <?php if ( $tinh_trang['value'] == "Hết hàng" )
										echo 'sold-out' ?>">
										<?php if ( $khuyenmai == true ) { ?>
                                            <div class="sale-label"><span>Sale</span></div>
										<?php } ?>
                                        <div class="product-image">
                                            <a href="<?= the_permalink() ?>"></a>
                                            <img src="<?= $hinhanh['hinh_chinh']['url'] ?>"
                                                 alt="<?= $hinhanh['hinh_chinh']['alt'] ?>">
                                        </div>
                                        <div class="product-item-details align-center">
                                            <div class="product-item-name"><a
                                                        href="<?= the_permalink() ?>"><?php the_title() ?></a>
                                            </div>
                                            <div class="price-box"><span
                                                        class="price">$<?= $gia_ban ?></span> <?php if ( ! $tt_sanpham['gia_goc'] == null ) { ?>
                                                    <del class="price old-price">
                                                        $<?= $gia_goc ?></del> <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php }
							wp_reset_query() ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div class="pagination-bar">
						<?php the_posts_pagination( array(
							'mid_size'  => 2,
							'prev_text' => __( '<', 'textdomain' ),
							'next_text' => __( '>', 'textdomain' ),
						) ); ?>
                    </div>
                </div>
            </div>
            <!-- TAB SECTION CONTENT ENDS -->
        </div>
        <!-- PRODUCT-LISTING MAIN CLASS START -->
    </div>
</section>
<!-- FEATURED PRODUCT AREA ENDS -->

<?php get_footer() ?>
