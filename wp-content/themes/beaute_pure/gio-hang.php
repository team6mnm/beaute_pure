<?php
/*
Template Name: Cart
*/

get_header( 'mini' );
?>



    <!-- BANNER STRAT -->
    <div class="banner inner-banner1">
        <div class="container">
            <section class="banner-detail ptb-95">
                <h1 class="banner-title">Shopping Cart</h1>
                <div class="bread-crumb">
                    <ul>
                        <li><a href="<?php bloginfo( 'url' ) ?>">Home</a>/</li>
                        <li><span>Shopping Cart</span></li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
    <!-- BANNER END -->


<?php
global $product_prefix;
$action = get_query_var( 'action' ); //thêm|xóa
$pro_id = get_query_var( 'pro_id' ); //id sản phẩm
if ( $action ) { //nếu có thao tác (thêm hoặc xóa)
	switch ( $action ) {
		case 'them':
			if ( isset( $_SESSION['cart'][ $pro_id ] ) ) //nếu đã có thì cập nhật số lượng thêm 1
			{
				$quantity = $_SESSION['cart'][ $pro_id ] + 1;
			} else {
				$quantity = 1;
			} //ngược lại tạo 1 item mới với số lượng là 1
			$_SESSION['cart'][ $pro_id ] = $quantity; //cập nhật lại
			wp_redirect( get_bloginfo( 'url' ) . '/gio-hang' );
			exit(); //trả về trang hiển thị giỏ hàng
			break;
		case 'xoa':
			if ( isset( $_SESSION['cart'] ) && count( $_SESSION['cart'] ) > 0 ) { //kiểm tra và xóa sản phẩm dựa vào id
				unset( $_SESSION['cart'][ $pro_id ] );
				wp_redirect( get_bloginfo( 'url' ) . '/gio-hang' );
				exit();
			} else {
				unset( $_SESSION['cart'] );
				echo "<h3>Hiện chưa có sản phẩm nào trong giỏ hàng! <a href='" . get_bloginfo( 'url' ) . "'>Bấm vào đây</a> để xem và mua hàng.</h3>";
			}
			break;
	}
} else { //không có thao tác thêm hoặc xóa thì sẽ hiển thị sản phẩm trong giỏ hàng
	if ( isset( $_SESSION['cart'] ) && count( $_SESSION['cart'] ) > 0 ) { //kiểm tra số lượng sản phẩm trước khi hiển thị
		?>
        <form action="" method="post">
            <!-- CONTAIN START -->
            <section class="ptb-95">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 mb-xs-30">
                            <div class="cart-item-table commun-table">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Sub Total</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
										<?php
										$total = 0;
										foreach ( $_SESSION['cart'] as $pro_id => $quantity ) { //lặp qua mảng cart session lấy ra id và show thông tin sản phẩm theo id đó
											$sanpham = new WP_Query( array( 'p' => $pro_id ) );
											while ( $sanpham->have_posts() ) {
												$sanpham->the_post();
												$hinhanh = get_field( 'hinhanh_sp' );
												$ha      = $hinhanh['hinh_chinh']['url'];
												$price   = get_field( 'gia_ban' );
												?>
                                                <tr>
                                                    <td><a href="product-page.html">
                                                            <div class="product-image"><img alt="anhsp"
                                                                                            src="<?= $ha ?>">
                                                            </div>
                                                        </a></td>
                                                    <td>
                                                        <div class="product-title"><a
                                                                    href="product-page.html"><?php the_title() ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <ul>
                                                            <li>
                                                                <div class="base-price price-box"><span
                                                                            class="price">$<?= $price ?></span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <div class="input-box">
                                                            <input autocomplete="off" type="text"
                                                                   value="<?php echo $quantity; ?>"
                                                                   name="quantity[<?php echo $pro_id; ?>]"/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="total-price price-box"><span
                                                                    class="price"><?php echo '$' . number_format( $price * $quantity ); ?>
																<?php $total += $price * $quantity; ?></span></div>
                                                    </td>
                                                    </td>
                                                    <td>
                                                        <a href="<?= get_bloginfo( 'url' ) . '/gio-hang/xoa/' . $pro_id ?>"><i
                                                                    title="Remove Item From Cart" data-id="100"
                                                                    class="fa fa-trash cart-remove-item"></i></a></td>
                                                </tr>
											<?php }
										} ?>
                                        <tr>
                                            <td>Amount Payable</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="price-box"><span class="price"><b>$<?= $total ?></b></span>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-30">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mt-30"><a href="<?php bloginfo( 'url' ) ?>" class="btn-color btn"><span><i
                                                    class="fa fa-angle-left"></i></span>Continue Shopping</a></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mt-30 right-side float-none-xs"><input class="btn-color btn"
                                                                                   type="submit" name="cart_update"
                                                                                   value="Update Cart"
                                                                                   title="Cập nhật giỏ hàng"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-30">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="right-side float-none-xs"><a href="#" class="btn-color btn">
                                        Proceed to checkout
                                        <span><i class="fa fa-angle-right"></i></span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- CONTAINER END -->
        </form>
		<?php
	} else {
		echo "<h3 style='text-align: center'>There are no products in your shopping cart! </h3><br><h2 style='text-align: center'><a style='color: #ff0735'  href='" . get_bloginfo( 'url' ) . "'>Click here</a> to continue shopping</h2>";
	}
	if ( isset( $_POST['cart_update'] ) ) { //xử lý cập nhật giỏ hàng
		if ( isset( $_POST['quantity'] ) ) {
			if ( count( $_SESSION['cart'] ) == 0 ) {
				unset( $_SESSION['cart'] );
			} //nếu không còn sản phẩm trong giỏ thì xóa giỏ hàng
			foreach ( $_POST['quantity'] as $pro_id => $quantity ) { //lặp mảng số lượng mới và cập nhật mới cho giỏ hàng
				if ( $quantity == 0 ) {
					unset( $_SESSION['cart'][ $pro_id ] );
				} else {
					$_SESSION['cart'][ $pro_id ] = $quantity;
				}
			}
			wp_redirect( get_bloginfo( 'url' ) . '/gio-hang' ); //cập nhật xong trả về trang hiển thị sản phẩm trong giỏ
		}
	}
}

get_footer();
?>