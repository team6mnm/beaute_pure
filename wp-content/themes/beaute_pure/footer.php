</div><!-- #content -->

<footer id="colophon" class="site-footer">
    <div class="footer">
        <div class="footer-inner">
            <div class="footer-bottom top">
                <div class="container">
                    <div class="col-sm-12 align-center">
                        <div class="copy-right center-xs">© 2018 by Beaute Pure | All Rights Reserved
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="payment left-side center-xs">
                                <ul class="payment_icon">
                                    <li class="visa"><a><img
                                                    src="<?php bloginfo( 'stylesheet_directory' ) ?>/images/pay1.png"
                                                    alt="Empire"></a></li>
                                    <li class="discover"><a><img
                                                    src="<?php bloginfo( 'stylesheet_directory' ) ?>/images/pay2.png"
                                                    alt="Empire"></a></li>
                                    <li class="paypal"><a><img
                                                    src="<?php bloginfo( 'stylesheet_directory' ) ?>/images/pay3.png"
                                                    alt="Empire"></a></li>
                                    <li class="vindicia"><a><img
                                                    src="<?php bloginfo( 'stylesheet_directory' ) ?>/images/pay4.png"
                                                    alt="Empire"></a></li>
                                    <li class="atos"><a><img
                                                    src="<?php bloginfo( 'stylesheet_directory' ) ?>/images/pay5.png"
                                                    alt="Empire"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="footer_social  center-xs right-side">
                                <ul class="social-icon">
                                    <li><a title="Facebook" class="facebook"><i class="fab fa-facebook"> </i></a></li>
                                    <li><a title="Twitter" class="twitter"><i class="fab fa-twitter"> </i></a></li>
                                    <li><a title="Linkedin" class="linkedin"><i class="fab fa-linkedin"> </i></a></li>
                                    <li><a title="RSS" class="rss"><i class="fas fa-rss"> </i></a></li>
                                    <li><a title="Pinterest" class="pinterest"><i class="fab fa-pinterest"> </i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

</body>
</html>
