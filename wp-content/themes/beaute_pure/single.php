<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package beaute_pure_v1.0
 */

get_header();
wp_reset_query();
$tinh_trang = get_field( 'tinh_trang' );
$khuyenmai  = get_field( 'km' );
$giaban     = get_field( 'gia_ban' );
$giagoc     = get_field( 'gia_goc' );
$code       = get_field( 'code' );
$trongluong = get_field( 'trongluong' );
$hinhanh    = get_field( 'hinhanh_sp' );
$nhan_hieu  = get_field( 'nhan_hieu' );
?>

    <div class="main">

        <!-- BANNER STRAT -->
        <div class="banner inner-banner1">
            <div class="container">
                <section class="banner-detail ptb-95">
                    <h1 class="banner-title">Product</h1>
                    <div class="bread-crumb">
                        <ul>
                            <li><a href="<?= bloginfo( 'home' ) ?>">Home</a>/</li>
                            <li><span>Product</span></li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
        <!-- BANNER END -->

        <!-- CONTAIN START -->
        <section class="pt-95">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4 col-sm-4 col-xs-5">
                            <div class="fotorama" data-nav="thumbs" data-allowfullscreen="native">
                                <a href="#">
                                    <img src="<?= $hinhanh['hinh_chinh']['url'] ?>"
                                         alt="<?= $hinhanh['hinh_chinh']['alt'] ?>">
                                </a>
                                <a href="#">
                                    <img src="<?= $hinhanh['hinh_2']['url'] ?>" alt="<?= $hinhanh['hinh_2']['alt'] ?>">
                                </a>
                                <a href="#">
                                    <img src="<?= $hinhanh['hinh_3']['url'] ?>" alt="<?= $hinhanh['hinh_3']['alt'] ?>">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="product-detail-main">
                                        <div class="product-item-details">
                                            <h1 class="product-item-name"><?= the_title() ?></h1>
                                            <br>
                                            <div class="price-box"><span
                                                        class="price">$<?= $giaban ?></span> <?php if ( ! $giagoc == null ) { ?>
                                                    <del class="price old-price">
                                                        $<?= $giagoc ?></del> <?php } ?> </div>
                                            <div class="product-info-stock-sku">
                                                <div>
                                                    <label>Availability: </label>
                                                    <span class="info-deta"><?= $tinh_trang ?></span></div>
                                                <div>
                                                    <label>CODE: </label>
                                                    <span class="info-deta"><?= $code ?></span></div>
                                                <div>
                                                    <label>Weight: </label>
                                                    <span class="info-deta"><?= $trongluong ?> g</span></div>
                                            </div>
                                            <div class="product-size select-arrow mb-20 mt-30">
                                                <p style="font-size: 20pt; font-weight: bold; color: #ff0735">
                                                    Trademark: <?= $nhan_hieu ?></p>
                                            </div>
                                            <div class="product-color select-arrow mb-20">
                                                <label>Color</label>
                                                <select class="selectpicker form-control" id="select-by-color">
													<?php $field = get_field_object( 'mau_sac' );
													$color       = $field['value'];
													foreach ( $color as $color ) { ?>
                                                        <option><?= $field['choices'][ $color ]; ?></option>
													<?php } ?>
                                                </select>
                                            </div>
                                            <p><?= get_post_field( 'post_content' ); ?></p>
                                            <div class="mb-20">
                                                <div class="bottom-detail cart-button">
                                                    <a href="<?php bloginfo( 'url' ) ?>/gio-hang/them/<?php the_ID(); ?>"
                                                       class="btn-color btn">
                                                        Add to Cart </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ptb-95">
            <div class="container">
                <div class="product-detail-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="tabs">
                                <ul class="nav nav-tabs">
                                    <li><a class="tab-Description selected" title="Description">Description</a></li>
                                    <li><a class="tab-Product-Tags" title="Product-Tags">Product-Tags</a></li>
                                </ul>
                            </div>
                            <div id="items">
                                <div class="tab_content">
                                    <ul>
                                        <li>
                                            <div class="items-Description selected ">
                                                <div class="Description">
	                                                <?= get_post_field( 'post_content' ); ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="items-Product-Tags">
												<?php the_tags( '', ', ', ',' ) ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- CONTAINER END -->

        <!-- FOOTER START -->
        <div class="footer">
            <div class="footer-inner">
                <div class="footer-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 f-col">
                                <div class="footer-static-block">
                                    <span class="opener plus"></span>
                                    <h3 class="title">Information</h3>
                                    <ul class="footer-block-contant link">
                                        <li><a>About Us</a></li>
                                        <li><a>Delivery Information</a></li>
                                        <li><a>Privacy Policy</a></li>
                                        <li><a>Terms & Conditions</a></li>
                                        <li><a>Gift Certificates</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 f-col">
                                <div class="footer-static-block">
                                    <span class="opener plus"></span>
                                    <h3 class="title">Help</h3>
                                    <ul class="footer-block-contant link">
                                        <li><a>About Us</a></li>
                                        <li><a>Delivery Information</a></li>
                                        <li><a>Privacy Policy</a></li>
                                        <li><a>Terms & Conditions</a></li>
                                        <li><a>Gift Certificates</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 f-col">
                                <div class="footer-static-block">
                                    <span class="opener plus"></span>
                                    <h3 class="title">Guidance</h3>
                                    <ul class="footer-block-contant link">
                                        <li><a>About Us</a></li>
                                        <li><a>Delivery Information</a></li>
                                        <li><a>Privacy Policy</a></li>
                                        <li><a>Terms & Conditions</a></li>
                                        <li><a>Gift Certificates</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 f-col">
                                <div class="footer-static-block">
                                    <span class="opener plus"></span>
                                    <h3 class="title">Contact</h3>
                                    <ul class="footer-block-contant address-footer">
                                        <li><p>Empire Ltd</p></li>
                                        <li><p>600 Anton Boulevard Costa</p></li>
                                        <li><p>Mesa, California</p></li>
                                        <li><p><a>info@empire.com</a></p></li>
                                        <li><p>029-222-3255-222</p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="footer-bottom top">
                    <div class="container">
                        <div class="col-sm-12 align-center">
                            <div class="copy-right center-xs">© 2018 All Rights Reserved. Design By <a href="#">HustleThemes009</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="payment left-side center-xs">
                                    <ul class="payment_icon">
                                        <li class="visa"><a><img src="images/pay1.png" alt="Empire"></a></li>
                                        <li class="discover"><a><img src="images/pay2.png" alt="Empire"></a></li>
                                        <li class="paypal"><a><img src="images/pay3.png" alt="Empire"></a></li>
                                        <li class="vindicia"><a><img src="images/pay4.png" alt="Empire"></a></li>
                                        <li class="atos"><a><img src="images/pay5.png" alt="Empire"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="footer_social pt-xs-15 center-xs mt-xs-15 right-side">
                                    <ul class="social-icon">
                                        <li><a title="Facebook" class="facebook"><i class="fa fa-facebook"> </i></a>
                                        </li>
                                        <li><a title="Twitter" class="twitter"><i class="fa fa-twitter"> </i></a></li>
                                        <li><a title="Linkedin" class="linkedin"><i class="fa fa-linkedin"> </i></a>
                                        </li>
                                        <li><a title="RSS" class="rss"><i class="fa fa-rss"> </i></a></li>
                                        <li><a title="Pinterest" class="pinterest"><i class="fa fa-pinterest"> </i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="scroll-top">
            <div id="scrollup"></div>
        </div>
        <!-- FOOTER END -->
    </div>

<?php
get_footer();
